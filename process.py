"""
Python3 script for portable book reader. Takes given plaintext book and
platform, encodes the text, attempts to compress it (if unable, leaves
uncompressed) and writes it into data.h.

usage: process.py booktext.txt platform

by Miloslav Ciz, 2019, released under CC0 1.0
"""

import sys
import subprocess
import unicodedata 

platforms = {
  # name       charsX charsY platform spec
  "terminal": (80,    20,    "c"),
  "sdl":      (40,    30,    "c"),
  "pokitto":  (35,    14,    "cpp"),
  "arduboy":  (21,    6,     "ino"),
  "gbmeta":   (20,    9,     "ino")
  }

PLATFORM = sys.argv[2]

MAX_CHARS_X = platforms[PLATFORM][0]
MAX_CHARS_Y = platforms[PLATFORM][1]

PAGE_SEPARATOR = 255

SPECIAL_CHAR_FROM = 128 - 32

pagesTotal = 1

NEW_LINE = 127 - 32

def encodeChar(c):
  if c == "\n":
    return NEW_LINE
  elif ord(c) < 32:
    return 0
  elif ord(c) > 127:
    return ord("?") - 32
  else:
    return ord(c) - 32

def decodeChar(d):
  return "\n" if d == NEW_LINE else chr(d + 32)

def encodeWithoutCompression(text):
  global pagesTotal

  data = []

  columnCount = 0
  rowCount = 0

  for i in range(len(text)):
    c = text[i]

    if c == "\n":
      data.append(NEW_LINE)
      columnCount = 0
      rowCount += 1
    else:
      if c != " ":
        wordLength = 0

        while (i + wordLength < len(text)) and text[i + wordLength] != " " and text[i + wordLength] != "\n":
          wordLength += 1

        if wordLength < MAX_CHARS_X and columnCount + wordLength > MAX_CHARS_X:
          data.append(NEW_LINE)
          columnCount = 0
          rowCount += 1

          if rowCount == MAX_CHARS_Y:
            data.append(PAGE_SEPARATOR)
            rowCount = 0
            pagesTotal += 1
      
        data.append(encodeChar(c))
        columnCount += 1
      else:
        if columnCount != 0 or (i > 0 and text[i - 1] == "\n"):
          data.append(encodeChar(c))
          columnCount += 1

    if columnCount == MAX_CHARS_X:
      data.append(NEW_LINE)
      columnCount = 0
      rowCount += 1

    if rowCount == MAX_CHARS_Y:
      data.append(PAGE_SEPARATOR)
      rowCount = 0
      columnCount = 0
      pagesTotal += 1

  pairDictionary = []
  
  return (data,pairDictionary)

def compress(text):
  encoded = encodeWithoutCompression(text)

  data = encoded[0]
  pairDictionary = encoded[1]

  for j in range(256 - SPECIAL_CHAR_FROM - 1):

    pairHistogram = {}

    for i in range(len(data) - 1):
      pair = (data[i],data[i + 1])

      if pair[0] != PAGE_SEPARATOR and pair[1] != PAGE_SEPARATOR:
        if not pair in pairHistogram:
          pairHistogram[pair] = 1
        else:
          pairHistogram[pair] += 1

    mostCommonCount = 0
    mostCommonPair = (0,0)

    for p in pairHistogram:
      if pairHistogram[p] > mostCommonCount:
        mostCommonCount = pairHistogram[p]
        mostCommonPair = p

    if mostCommonCount == 0:
      break

    pairDictionary.append(mostCommonPair)

    # replace

    pos = 0

    while pos < len(data) - 1:
      if data[pos] == mostCommonPair[0] and data[pos + 1] == mostCommonPair[1]:
        data[pos] = SPECIAL_CHAR_FROM + j
        del data[pos + 1]
      else:
        pos += 1
  
  return (data,pairDictionary)

def expandCode(code,pairDictionary):
  p = pairDictionary[code - SPECIAL_CHAR_FROM]

  return (
    (decodeChar(p[0]) if p[0] < SPECIAL_CHAR_FROM else expandCode(p[0],pairDictionary)) +
    (decodeChar(p[1]) if p[1] < SPECIAL_CHAR_FROM else expandCode(p[1],pairDictionary)))

def decompress(data,pairDictionary):
  text = ""

  for d in data:
    if d < SPECIAL_CHAR_FROM:
      text += decodeChar(d)
    elif d == PAGE_SEPARATOR:
      text += "\n\n------------ NEW PAGE ------------\n\n"
    else:
      text += expandCode(d,pairDictionary)

  return text

def arrayToStr(a):
  result = "  "

  count = 0

  for i in a:
    result += str(i) + ","
    count += 1
 
    if count > 20:
      result += "\n  "
      count = 0

  return result[:-1]

def formatCompressedData(data,pairDictionary):
  a = []

  for p in pairDictionary:
    a.append(p[0])
    a.append(p[1])

  result =  "#define BRD_PLATFORM_HEADER \"platform_" + PLATFORM + ".h\"\n\n"
  result += "#define BRD_CHARS_X " + str(MAX_CHARS_X) + "\n"
  result += "#define BRD_CHARS_Y " + str(MAX_CHARS_Y) + "\n\n"
  result += "#define BRD_PAGE_COUNT " + str(pagesTotal) + "\n\n"
  result += "#define BRD_COMPRESSION_DICTIONARY_SIZE " + str(len(a)) + "\n"
  result += "BRD_PROGRAM_MEMORY uint8_t BRD_compressionDictionary[BRD_COMPRESSION_DICTIONARY_SIZE] = {\n"

  result += arrayToStr(a)
  result += "};\n\n#define BRD_COMPRESSED_DATA_SIZE " + str(len(data)) + "\n"
  result += "BRD_PROGRAM_MEMORY uint8_t BRD_compressedData[BRD_COMPRESSED_DATA_SIZE] = {\n"
  result += arrayToStr(data)
  result += "};\n"
  return result

def sanitizeText(t):
  text = unicodedata.normalize("NFKD",t).replace("−","-").replace("—","-")
  
  result = ""

  for c in text:
    if ord(c) < 128:
      result += c

  return result

#------------------------

print("opening file " + sys.argv[1])

with open(sys.argv[1],"r") as file:
  text = sanitizeText(file.read())

print("compressing")
d = compress(text)

outFile = open("data.h","w")

originalSize = len(text)
newSize = len(d[0]) + len(d[1]) * 2

if newSize < originalSize:
  print("compressed: " + str(originalSize) + "B --> " + str(newSize) + "B (" + str(int(newSize / float(originalSize) * 100)) + "%)")
  outFile.write("// original size: " + str(originalSize) + ", compressed size: " + str(newSize) + "\n")
else:
  d = encodeWithoutCompression(text)
  print("couldn't compress, leaving uncompressed")
  outFile.write("// not compressed\n")
  
outFile.write(formatCompressedData(d[0],d[1]))

print("data written to data.h")
print("total pages: " + str(pagesTotal))

outFile.close()

print("writing preview to preview.txt")

text = decompress(d[0],d[1])

outFile = open("preview.txt","w")
outFile.write(text)
outFile.close()

print("copying files to output directory")

subprocess.call(["rm", "-rf", "output"])
subprocess.call(["mkdir", "output"])
subprocess.call(["cp", "main_" + platforms[PLATFORM][2] + "." + platforms[PLATFORM][2],"output"])
subprocess.call(["cp", "core.h","output"])
subprocess.call(["cp", "data.h","output"])
subprocess.call(["cp", "font.h","output"])
subprocess.call(["cp", "platform_" + PLATFORM + ".h","output"])

print("done")
