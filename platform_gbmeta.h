/**
  Gamebuino META front end for book reader.

  by Miloslav Ciz, released under CC0 1.0
*/

#include <Gamebuino-Meta.h>

#define SAVE_VALID 112455

uint8_t bookmarkTimer = 0;

void setup()
{
  gb.begin();

  BRD_init();

  int32_t n = gb.save.get(0);

  if (n != SAVE_VALID)
  {
    gb.save.set(0,SAVE_VALID);
    gb.save.set(1,(int32_t) 0);
  }
  else
    BRD_decodePage(gb.save.get(1));

  bookmarkTimer = 0;
}

int buttonRegisters(Gamebuino_Meta::Button button)
{
  int t = gb.buttons.timeHeld(button);

  return t == 1 || (t > 8 && t % 4 == 0);
}

void loop()
{
  while(!gb.update());

  gb.display.clear();

  gb.display.setColor(GRAY);

  gb.display.setCursor(0,0);
  gb.display.print((char *) BRD_pageArray);

  gb.display.setCursor(0,56);

  if (bookmarkTimer > 0)
  {
    bookmarkTimer--;

    gb.display.print("bookmark saved");
  }
  else
  {
    gb.display.print(BRD_currentPage);
    gb.display.print(" / ");
    gb.display.print(BRD_PAGE_COUNT);
  }

  if (buttonRegisters(BUTTON_RIGHT))
    BRD_nextPage();
  else if (buttonRegisters(BUTTON_LEFT))
    BRD_previousPage();
  else if (buttonRegisters(BUTTON_DOWN))
    BRD_offsetPage(10);
  else if (buttonRegisters(BUTTON_UP))
    BRD_offsetPage(-10);
  else if (gb.buttons.timeHeld(BUTTON_A) == 1)
  {
    gb.save.set(1,BRD_currentPage);
    bookmarkTimer = 20;
  }
}
