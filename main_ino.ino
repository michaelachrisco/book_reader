#include <avr/pgmspace.h>

#define BRD_PROGRAM_MEMORY const PROGMEM

#define BRD_PLATFORM_ARDUINO 1

#define BRD_GET_PROGRAM_MEMORY_BYTE(addr,offset) (pgm_read_byte_near((addr) + (offset)))
#define BRD_GET_PROGRAM_MEMORY_WORD(addr,offset) (pgm_read_word_near((addr) + (offset)))

#include "core.h"
